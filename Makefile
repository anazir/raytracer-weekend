P=main
OBJECTS=camera.o  color.o  hit_record.o  hittable_list.o  material.o  ray.o  rtweekend.o  sphere.o  vec3.o
CFLAGS = -g -Wall -O0 -lm `pkg-config --cflags glib-2.0`
LDLIBS= `pkg-config --libs glib-2.0`
CC=c99

$(P): $(OBJECTS)
