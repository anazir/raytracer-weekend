#include "camera.h"
#include "rtweekend.h"
#include <stdlib.h>
#include <math.h>

camera* new_camera(point3 lookfrom, point3 lookat, vec3 vup, double vfov, double aspect_ratio, double aperture, double focus_dist) {
  camera *c = (camera*) malloc(sizeof(camera));

  double theta = degrees_to_radians(vfov);
  double h = tan(theta / 2);
  double viewport_height = 2.0 * h;
  double viewport_width = aspect_ratio * viewport_height;

  c->w = unit_vector(sub(lookfrom, lookat));
  c->u = unit_vector(cross(vup, c->w));
  c->v = cross(c->w, c->u);

  c->origin = lookfrom;
  c->horizontal = mult_dv(focus_dist, mult_dv(viewport_width, c->u));
  c->vertical = mult_dv(focus_dist, mult_dv(viewport_height, c->v));
  c->lower_left_corner = sub(sub(sub(c->origin, div_vd(c->horizontal, 2)), div_vd(c->vertical, 2)), mult_dv(focus_dist, c->w));

  c->lens_radius = aperture / 2;

  return c;
}

ray get_ray(camera *c, double u, double v) {
  vec3 rd = mult_dv(c->lens_radius, random_vec_in_unit_disk());
  vec3 offset = add(mult_vd(c->u, x(rd)), mult_vd(c->v, y(rd)));
  return new_ray(add(offset, c->origin), sub(sub(add(add(c->lower_left_corner, mult_dv(u, c->horizontal)), mult_dv(v, c->vertical)), c->origin), offset));
}
