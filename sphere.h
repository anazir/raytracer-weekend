#ifndef SPHERE_H
#define SPHERE_H

#include "hittable.h"

typedef struct sphere {
  hittable base;
  point3 center;
  double radius;
  material mat_ptr;
} sphere;

int hit_sphere(hittable *h, const ray r, double t_min, double t_max, hit_record *rec);
sphere* new_sphere(point3 cen, double r, material m);

#endif
