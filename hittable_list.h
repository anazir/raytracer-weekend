#ifndef HITTABLE_LIST_H
#define HITTABLE_LIST_H

#include "hittable.h"
#include <glib.h>

typedef struct hittable_list {
  hittable base;
  GList *objects;
} hittable_list;

int hit_hittable_list(hittable *h, const ray r, double t_min, double t_max, hit_record *rec);
hittable_list* new_hittable_list(hittable *h);
void add_hittable_list(hittable_list l, hittable *h);

#endif
