#include "ray.h"

ray new_ray(const point3 origin, const vec3 direction) {
  ray r;
  r.orig = clone(origin);
  r.dir = clone(direction);

  return r;
}

point3 ray_at(ray r, double t) {
  return add(r.orig, mult_dv(t, r.dir));
}
