#include "material.h"
#include "rtweekend.h"
#include <stdlib.h>
#include <math.h>

int scatter_lambertian(material *m, const ray *r_in, const struct hit_record rec, color *attenuation, ray *scattered) {
  lambertian *l = (lambertian*)m->hook;
  vec3 scater_direction = add(rec.normal, random_unit_vector());

  // Catch degenarate scatter direction
  if (near_zero(scater_direction))
    scater_direction = rec.normal;

  *scattered = new_ray(rec.p, scater_direction);
  *attenuation = l->albedo;
  return 1;
}

lambertian* new_lambertian(const color *a) {
  lambertian *l = (lambertian*) malloc(sizeof(lambertian));
  l->base.scatter = scatter_lambertian;
  l->base.hook = l;
  l->albedo = clone(*a);

  return l;
}

int scatter_metal(material *m, const ray *r_in, const struct hit_record rec, color *attenuation, ray *scattered) {
  metal *l = (metal*)m->hook;
  vec3 reflected = reflect(unit_vector(r_in->dir), rec.normal);
  *scattered = new_ray(rec.p, add(reflected, mult_dv(l->fuzz, random_vec_in_unit_sphere())));
  *attenuation = l->albedo;
  return (dot(scattered->dir, rec.normal) > 0);
}

metal* new_metal(const color *a, double f) {
  metal *l = (metal*) malloc(sizeof(metal));
  l->base.scatter = scatter_metal;
  l->base.hook = l;
  l->albedo = clone(*a);
  l->fuzz = f < 1 ? f : 1;

  return l;
}

double reflectance(double cosine, double ref_idx) {
  // Use Schlick's approximation for reflectance
  double r0 = (1 - ref_idx) / (1 + ref_idx);
  r0 = r0 * r0;
  return r0 + (1 - r0) * pow(1 - cosine, 5);
}

int scatter_dielectric(material *m, const ray *r_in, const struct hit_record rec, color *attenuation, ray *scattered) {
  dielectric *l = (dielectric*)m->hook;
  *attenuation = new_vec(1, 1, 1);
  double refraction_ratio = rec.front_face ? (1.0 / l->ir) : l->ir;

  vec3 unit_direction = unit_vector(r_in->dir);
  double cos_theta = fmin(dot(neg(unit_direction), rec.normal), 1);
  double sin_theta = sqrt(1.0 - cos_theta * cos_theta);

  int cannot_refract = refraction_ratio * sin_theta > 1.0;
  vec3 direction;

  if(cannot_refract || reflectance(cos_theta, refraction_ratio) > random_double())
    direction = reflect(unit_direction, rec.normal);
  else
    direction = refract(unit_direction, rec.normal, refraction_ratio);

  *scattered = new_ray(rec.p, direction);
  return 1;
}

dielectric* new_dielectric(double index_of_refraction) {
  dielectric *l = (dielectric*) malloc(sizeof(dielectric));
  l->base.scatter = scatter_dielectric;
  l->base.hook = l;
  l->ir = index_of_refraction;

  return l;
}

