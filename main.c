#include "rtweekend.h"
#include "color.h"
#include "hittable_list.h"
#include "sphere.h"
#include "camera.h"

#include <stdio.h>

color ray_color(const ray r, hittable world, int depth) {
  if(depth <= 0)
    return new_vec(0, 0, 0);

  hit_record *rec = (hit_record*) malloc(sizeof(hit_record));

  if (world.hit(&world, r, 0.001, infinity, rec)) {
    ray *scattered = (ray*) malloc(sizeof(ray));
    color attenuation;

    if (rec->mat_ptr->scatter(rec->mat_ptr, &r, *rec, &attenuation, scattered)) {
      color to_return = mult_vv(attenuation, ray_color(*scattered, world, depth - 1));

      free(scattered);
      free(rec);
      return to_return;
    }

    free(rec);
    free(scattered);
    return new_vec(0, 0, 0);
  }

  free(rec);

  vec3 unit_direction = unit_vector(r.dir);
  double t = .5 * (y(unit_direction) + 1.0);
  return add(mult_dv((1.0 - t), new_vec(1, 1, 1)), mult_dv(t, new_vec(.5, .7, 1)));
}

hittable_list* random_scene() {
  color c1 = new_vec(.5, .5, 5);
  lambertian *ground_material = new_lambertian(&c1);
  sphere *ground = new_sphere(new_vec(0.0, -1000, 0), 1000, (ground_material->base));
  hittable_list *world = new_hittable_list(&(ground->base));

  for (int a = -11; a < 11; a += 2) {
    for (int b = -11; b < 11; b += 2) {
      double choose_mat = random_double();
      point3 center = new_vec(a + .9 * random_double(), .2, b + .9 * random_double());

      if (length(sub(center, new_vec(4, .2, 0))) > .9) {
        material *sphere_material;

        if(choose_mat < .8) {
          // diffuse
          color albedo = mult_vv(random_vec(), random_vec());
          lambertian *temp = new_lambertian(&albedo);
          sphere_material = &temp->base;
          sphere *s = new_sphere(center, .2, *sphere_material);
          add_hittable_list(*world, &s->base);
        } else if(choose_mat < .95) {
          // metal
          color albedo = random_vec_limits(.5, 1);
          double fuzz = random_double_limits(0, .5);
          metal *temp = new_metal(&albedo, fuzz);
          sphere_material = &temp->base;
          sphere *s = new_sphere(center, .2, *sphere_material);
          add_hittable_list(*world, &s->base);
        } else {
          // glass
          dielectric *temp = new_dielectric(1.5);
          sphere_material = &temp->base;
          sphere *s = new_sphere(center, .2, *sphere_material);
          add_hittable_list(*world, &s->base);
        }
      }
    }
  }

  dielectric *material1 = new_dielectric(1.5);
  sphere *s1 = new_sphere(new_vec(0, 1, 0), 1, (material1->base));
  add_hittable_list(*world, &s1->base);

  color c2 = new_vec(.4, .2, .1);
  lambertian *material2 = new_lambertian(&c2);
  sphere *s2 = new_sphere(new_vec(-4, 1, 0), 1, (material2->base));
  add_hittable_list(*world, &s2->base);

  color c3 = new_vec(.7, .6, .5);
  metal *material3 = new_metal(&c3, 0);
  sphere *s3 = new_sphere(new_vec(4, 1, 0), 1, (material3->base));
  add_hittable_list(*world, &s3->base);

  return world;
}

int main() {
  // Image
  const double aspect_ratio = 3.0 / 2.0;
  const int image_width = 400;
  const int image_height = (int) (image_width / aspect_ratio);
  const int samples_per_pixel = 50;
  const int max_depth = 50;

  // World
  hittable_list *world = random_scene();

  // Camera
  point3 lookfrom = new_vec(13, 2, 3);
  point3 lookat = new_vec(0, 0, 0);
  point3 vup = new_vec(0, 1, 0);
  double dist_to_focus = 10;
  double aperture = .1;

  camera *cam = new_camera(lookfrom, lookat, vup, 20, aspect_ratio, aperture, dist_to_focus);

  // Render
  printf("P3\n%d %d\n255\n", image_width, image_height);

  for (int j = image_height - 1; j >= 0; --j) {
    fprintf(stderr, "\rScanlines remaining: %d ", j);

    for (int i = 0; i < image_width; ++i) {
      color pixel_color = new_vec(0, 0, 0);

      for(int s = 0; s < samples_per_pixel; ++s) {
        double u = (double)((i + random_double()) / (image_width - 1));
        double v = (double)((j + random_double()) / (image_height - 1));
        ray r = get_ray(cam, u, v);
        addE(&pixel_color, ray_color(r, world->base, max_depth));
      }
      write_color(stdout, pixel_color, samples_per_pixel);
    }
  }

  free(cam);
  g_list_free_full(world->objects, g_free);
  free(world);
}
