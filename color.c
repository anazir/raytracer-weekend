#include "color.h"
#include "rtweekend.h"
#include <math.h>

void write_color(FILE *str, color pixel_color, int samples_per_pixel) {
  double r = x(pixel_color);
  double g = y(pixel_color);
  double b = z(pixel_color);

  // Divide the color by the number of samples
  double scale = 1.0 / samples_per_pixel;
  r = sqrt(scale * r);
  g = sqrt(scale * g);
  b = sqrt(scale * b);

  // Write the translated [0, 255] value of each color component
  fprintf(str, "%d %d %d\n",
      (int)(256 * clamp(r, 0.0, 0.999)),
      (int)(256 * clamp(g, 0.0, 0.999)),
      (int)(256 * clamp(b, 0.0, 0.999)));
}
