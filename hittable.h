#ifndef HITTABLE_H
#define HITTABLE_H

#include "hit_record.h"
#include "ray.h"

typedef struct hittable {
  int (*hit)(struct hittable *h, const ray r, double t_min, double t_max, hit_record *rec);
  void * hook;
} hittable;

#endif
