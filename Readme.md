# Ray tracing in C

Implementation of the ray tracer from [here](https://raytracing.github.io/books/RayTracingInOneWeekend.html#addingasphere/creatingourfirstraytracedimage)

![My final render](render.png)
