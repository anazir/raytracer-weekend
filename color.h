#ifndef COLOR_H
#define COLOR_H

#include "vec3.h"

void write_color(FILE *str, color pixel_color, int samples_per_pixel);

#endif
