#include "hittable_list.h"

int hit_hittable_list(hittable *h, const ray r, double t_min, double t_max, hit_record *rec) {
  hittable_list *s = (hittable_list*)h->hook;
  hit_record *temp_rec = (hit_record*) malloc(sizeof(hit_record));
  int hit_anything = 0;
  double closest_so_far = t_max;

  for (int i = 0; i < g_list_length(s->objects); i++) {
    hittable *cur = g_list_nth_data(s->objects, i);

    if (cur->hit(cur, r, t_min, closest_so_far, temp_rec)) {
      hit_anything = 1;
      closest_so_far = temp_rec->t;
      *rec = *temp_rec;
    }
  }

  free(temp_rec);

  return hit_anything;
}

hittable_list* new_hittable_list(hittable *h) {
  hittable_list *l = (hittable_list*) malloc(sizeof(hittable_list));
  l->base.hit = hit_hittable_list;
  l->base.hook = l;
  GList *objects = NULL;
  objects = g_list_append(objects, h);
  l->objects = objects;

  return l;
}

void add_hittable_list(hittable_list l, hittable *h) {
  l.objects = g_list_append(l.objects, h);
}
