#ifndef RAY_H
#define RAY_H

#include "vec3.h"

typedef struct ray {
  point3 orig;
  vec3 dir;
} ray;

ray new_ray(const point3 origin, const vec3 direction);
point3 ray_at(ray r, double t);

#endif
