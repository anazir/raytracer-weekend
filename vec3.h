#ifndef VEC3_H
#define VEC3_H

#include<stdio.h>

typedef struct vec3 {
  double e[3];
} vec3;

vec3 clone(vec3 a);
vec3 new_vec(double x, double y, double z);
vec3 random_vec();
vec3 random_vec_limits(double min, double max);
double x(vec3 a);
double y(vec3 a);
double z(vec3 a);
vec3 neg(vec3 a);
double ind(vec3 a, int i);
void addE(vec3 *a, vec3 b);
void multE(vec3 a, double b);
void divE(vec3 a, double b);
double length_squared(vec3 a);
double length(vec3 a);

typedef vec3 point3;
typedef vec3 color;

// Utility functions
void outstream(FILE *str, const vec3 v);
vec3 add(vec3 a, vec3 b);
vec3 sub(vec3 a, vec3 b);
vec3 mult_vv(vec3 a, vec3 b);
vec3 mult_dv(double t, vec3 b);
vec3 mult_vd(vec3 b, double t);
vec3 div_vd(vec3 b, double t);
double dot(vec3 a, vec3 b);
vec3 cross(vec3 a, vec3 b);
vec3 unit_vector(vec3 v);
vec3 random_vec_in_unit_sphere();
vec3 random_vec_in_unit_disk();
vec3 random_unit_vector();
int near_zero(vec3 a);
vec3 reflect(const vec3 v, const vec3 n);
vec3 refract(const vec3 uv, const vec3 n, double etai_over_etat);

#endif
