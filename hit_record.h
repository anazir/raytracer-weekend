#ifndef HIT_RECORD_H
#define HIT_RECORD_H

#include "ray.h"
#include "material.h"

typedef struct hit_record {
  point3 p;
  vec3 normal;
  struct material *mat_ptr;
  double t;
  int front_face;
} hit_record;

void set_face_normal(hit_record *h, const ray r, const vec3 outward_normal);

#endif
