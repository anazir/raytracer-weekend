#include "vec3.h"
#include<math.h>
#include "rtweekend.h"

vec3 clone(vec3 a) {
  vec3 v;
  v.e[0] = a.e[0];
  v.e[1] = a.e[1];
  v.e[2] = a.e[2];

  return v;
}

vec3 new_vec(double x, double y, double z) {
  vec3 new_vec3;
  new_vec3.e[0] = x;
  new_vec3.e[1] = y;
  new_vec3.e[2] = z;

  return new_vec3;
}

vec3 random_vec() {
  return new_vec(random_double(), random_double(), random_double());
}

vec3 random_vec_limits(double min, double max) {
  return new_vec(random_double_limits(min, max), random_double_limits(min, max), random_double_limits(min, max));
}

double x(vec3 a) { return a.e[0]; }
double y(vec3 a) { return a.e[1]; }
double z(vec3 a) { return a.e[2]; }

vec3 neg(vec3 a) { return new_vec(-a.e[0], -a.e[1], -a.e[2]); }
double ind(vec3 a, int i) { return a.e[i]; }

void addE(vec3 *a, vec3 b) {
  a->e[0] += b.e[0];
  a->e[1] += b.e[1];
  a->e[2] += b.e[2];
}

void multE(vec3 a, double b) {
  a.e[0] *= b;
  a.e[1] *= b;
  a.e[2] *= b;
}

void divE(vec3 a, double b) {
  multE(a, 1/b);
}

double length_squared(vec3 a) {
  return a.e[0] * a.e[0] + a.e[1] * a.e[1] + a.e[2] * a.e[2];
}

double length(vec3 a) {
  return sqrt(length_squared(a));
}

// Utility functions
void outstream(FILE *str, const vec3 v) {
  fprintf(str, "%f %f %f", v.e[0], v.e[1], v.e[2]);
}

vec3 add(vec3 a, vec3 b) {
  return new_vec(a.e[0] + b.e[0], a.e[1] + b.e[1], a.e[2] + b.e[2]);
}

vec3 sub(vec3 a, vec3 b) {
  return new_vec(a.e[0] - b.e[0], a.e[1] - b.e[1], a.e[2] - b.e[2]);
}

vec3 mult_vv(vec3 a, vec3 b) {
  return new_vec(a.e[0] * b.e[0], a.e[1] * b.e[1], a.e[2] * b.e[2]);
}

vec3 mult_dv(double t, vec3 b) {
  return new_vec(t * b.e[0], t * b.e[1], t * b.e[2]);
}

vec3 mult_vd(vec3 b, double t) {
  return new_vec(t * b.e[0], t * b.e[1], t * b.e[2]);
}

vec3 div_vd(vec3 b, double t) {
  return mult_dv(1 / t, b);
}

double dot(vec3 a, vec3 b) {
  return a.e[0] * b.e[0] + a.e[1] * b.e[1] + a.e[2] * b.e[2];
}

vec3 cross(vec3 a, vec3 b) {
  return new_vec( a.e[1] * b.e[2] - a.e[2] * b.e[1],
                  a.e[2] * b.e[0] - a.e[0] * b.e[2],
                  a.e[0] * b.e[1] - a.e[1] * b.e[0]);
}

vec3 unit_vector(vec3 v) {
  return div_vd(v, length(v));
}

vec3 random_vec_in_unit_sphere() {
  while(1) {
    vec3 p = random_vec_limits(-1, 1);
    if (length_squared(p) >= 1) continue;
    return p;
  }
}

vec3 random_vec_in_unit_disk() {
  while(1) {
    vec3 p = new_vec(random_double_limits(-1, 1), random_double_limits(-1, 1), 0);
    if (length_squared(p) >= 1) continue;
    return p;
  }
}

vec3 random_unit_vector() {
  return unit_vector(random_vec_in_unit_sphere());
}

int near_zero(vec3 a) {
  const double s = 1e-8;
  return (fabs(a.e[0]) < s) && (fabs(a.e[1]) < s) && (fabs(a.e[2]) < s);
}

vec3 reflect(const vec3 v, const vec3 n) {
  return sub(v, mult_dv(2 * dot(v, n), n));
}

vec3 refract(const vec3 uv, const vec3 n, double etai_over_etat) {
  double cos_theta = fmin(dot(neg(uv), n), 1);
  vec3 r_out_perp = mult_dv(etai_over_etat, add(uv, mult_dv(cos_theta, n)));
  vec3 r_out_parallel = mult_dv(-sqrt(fabs(1 - length_squared(r_out_perp))), n);

  return add(r_out_perp, r_out_parallel);
}
