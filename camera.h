#ifndef CAMERA_H
#define CAMERA_H

#include "ray.h"

typedef struct camera {
  point3 origin;
  point3 lower_left_corner;
  vec3 horizontal;
  vec3 vertical;
  vec3 u, v, w;
  double lens_radius;
} camera;

camera* new_camera(point3 lookfrom, point3 lookat, vec3 vup, double vfov, double aspect_ratio, double aperture, double focus_disc);
ray get_ray(camera *c, double u, double v);

#endif
