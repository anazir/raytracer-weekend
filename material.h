#ifndef MATERIAL_H
#define MATERIAL_H

typedef struct material material;

#include "hit_record.h"

struct hit_record;
struct material {
  int (*scatter)(struct material *m, const ray *r, struct hit_record rec, color *attenuation, ray *scattered);
  void * hook;
};

typedef struct lambertian {
  material base;
  color albedo;
} lambertian;

typedef struct metal {
  material base;
  color albedo;
  double fuzz;
} metal;

typedef struct dielectric {
  material base;
  double ir;
} dielectric;

int scatter_lambertian(material *m, const ray *r_in, struct hit_record rec, color *attenuation, ray *scattered);
lambertian* new_lambertian(const color *a);
int scatter_metal(material *m, const ray *r_in, struct hit_record rec, color *attenuation, ray *scattered);
metal* new_metal(const color *a, double fuzz);
int scatter_dielectric(material *m, const ray *r_in, struct hit_record rec, color *attenuation, ray *scattered);
dielectric* new_dielectric(double index_of_refraction);

#endif
