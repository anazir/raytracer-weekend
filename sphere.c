#include "sphere.h"
#include <stdlib.h>
#include <math.h>

int hit_sphere(hittable *h, const ray r, double t_min, double t_max, hit_record *rec) {
  sphere *s = (sphere*)h->hook;
  vec3 oc = sub(r.orig, s->center);
  double a = length_squared(r.dir);
  double half_b = dot(oc, r.dir);
  double c = length_squared(oc) - s->radius * s->radius;

  double discriminant = half_b * half_b - a * c;
  if (discriminant < 0) return 0;
  double sqrtd = sqrt(discriminant);

  //Find the nearest root that lies in the acceptable range
  double root = (-half_b - sqrtd) / a;
  if (root < t_min || t_max < root) {
    root = (-half_b + sqrtd) / a;
    if (root < t_min || t_max < root)
      return 0;
  }

  rec->t = root;
  rec->p = ray_at(r, rec->t);
  vec3 outward_normal = unit_vector(div_vd(sub(rec->p, s->center), s->radius));
  set_face_normal(rec, r, outward_normal);
  rec->mat_ptr = &s->mat_ptr;

  return 1;
}

sphere* new_sphere(point3 cen, double r, material m) {
  sphere *s = (sphere*) malloc(sizeof(sphere));
  s->base.hit = hit_sphere;
  s->base.hook = s;
  s->center = clone(cen);
  s->radius = r;
  s->mat_ptr = m;

  return s;
}
