#include "hit_record.h"

void set_face_normal(hit_record *h, const ray r, const vec3 outward_normal) {
  h->front_face = dot(r.dir, outward_normal) < 0;
  h->normal = h->front_face ? outward_normal : neg(outward_normal);
}

