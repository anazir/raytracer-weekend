#ifndef RTWEEKEND_H
#define RTWEEKEND_H

// Constants

extern const double infinity;
extern const double pi;

// Utility Functions
double degrees_to_radians(double degrees);
double random_double();
double random_double_limits(double min, double max);
double clamp(double x, double min, double max);

// Common Headers

#include "ray.h"
#include "vec3.h"

#endif
